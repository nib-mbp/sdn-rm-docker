
# SDN Replication Manager <!-- omit in toc -->

- [General](#general)
  - [Glossary](#glossary)
- [Dockerizing Tomcat and Replication Manager](#dockerizing-tomcat-and-replication-manager)
  - [Docker persistent volumes](#docker-persistent-volumes)
    - [Data backup](#data-backup)
  - [Docker Compose (Container Specification)](#docker-compose-container-specification)
  - [Starting the `sdn-rm` container](#starting-the-sdn-rm-container)
- [Basic Configuration](#basic-configuration)
  - [Network ACL](#network-acl)
  - [Apache Reverse-Proxy](#apache-reverse-proxy)
- [References](#references)
- [Reverse-Proxy Support](#reverse-proxy-support)
  - [Apache config](#apache-config)

---

## General

This guide describes an actual working setup of *SeaDataNet Replication Manager* (hereafter referred to as *SDN RM* or only *RM*) running in a *Docker* container.

### Glossary

| Term  | Description |
|---|---|
| SDN | Sea Data Net |
| RM | Replication Manager |
| `.war` | Java Web Archive, i.e. the *RM* distribution format to be deployed on a *Java Servlet Container* such as *Apache Tomcat 8* |

## Dockerizing Tomcat and Replication Manager

If *RM* is going to be deployed on a shared server, its dependencies to *Tomcat 8+* and *JRE 1.8* could pose problems. Such problems can be avoided by deploying *RM* into Docker container. Similar procedure can be further evolved to deploy *RM* to Kubernetes culster.

*RM* requires *Tomcat 8+* and *JRE 1.8*. Therefore the `tomcat:8-jdk8` Docker Hub image can be used. As *RM* configuration is packed within the distributed `.war` file, the containers cannot be made immutable, but they should use persistence. Fort his Docker named volumes volumes are going to be used.

Next used `docker-compose` is used for setting up the *RM Docker container*.

**NOTE:** Such deployment impose *RM* sitting behind a load-balancer or a simple reverse-proxy. For this reason, some tweaking to access rules is needed, which will be described below. In order to simplify the deployment, the default Docker `bridge` network is going to be used - in my environment this resulted in the internal IP being `172.17.0.1`. Details on actual configuration can be inspected by running `docker network inspect bridge`.

### Docker persistent volumes

Three persistent volumes are used by this deployment as follows:

- `sdnrm_rm-home` **&rightarrow;** RM application home directory - it contains application specific files. <br>
  Container path: `/home/ReplicationManager` .
- `sdnrm_tomcat-webapps` &rightarrow; used as Tomcat deployment location, i.e. the *RM* binary `.war` package will reside there and upon deployment, it will be extracted there together with the web application configuration `WEB-INF/web.xml`. <br>
  Container path: `/usr/local/tomcat/webapps` .
- `sdnrm_tomcat-logs` &rightarrow; contains Tomcat logs.
  Container path: `/usr/local/tomcat/logs` .

#### **Volumes creation** <!-- omit in toc -->

```bash
# sdnrm_rm-home (local path: /srv/SDN_RM/rm-home)
docker volume create --name sdnrm_rm-home \
    --driver local \
    --opt device=/srv/SDN_RM/rm-home

# sdnrm_tomcat-webapps (local path: /srv/SDN_RM/webapps)
docker volume create --name sdnrm_tomcat-webapps \
    --driver local \
    --opt device=/srv/SDN_RM/webapps

# sdnrm_tomcat-logs (local path: /srv/SDN_RM/logs)
docker volume create --name sdnrm_tomcat-logs \
    --driver local \
    --opt device=/srv/SDN_RM/logs
```

#### Data backup

It makes sense to backup RM configuration and data from time to time and before introducing significant changes, like upgrades or similar. Here is a simple script for doing backups:

```bash
#!/bin/bash

if [ ! -d /srv/SDN_RM ]; then
  mkdir -p /srv/SDN_RM
fi
cd /srv/SDN_RM

dt=$(date +"%Y%m%d_%H%M")
base_dir=$(pwd)
mkdir -p __backup__/${dt}
tar -C webapps/ReplicationManager -f ${base_dir}/__backup__/${dt}/WEB-INF.tar -p -c WEB-INF
tar -f ${base_dir}/__backup__/${dt}/rm-home.tar.gz -z -p -c rm-home

cat <<_EOF_

----------------
Backup archives:
_EOF_
ls -alh ${base_dir}/__backup__/${dt}/*.tar*
echo ""
```

### Docker Compose (Container Specification)

Docker `docker-compose` utility is used for containerise *RM* application. The utility takes as imput creation instructions specified in a `docker-compose.yml` file ([reference manual](https://docs.docker.com/compose/compose-file/)).

Here is the `docker-compose.yml` used for our environment :

```yaml
version: '3.3'

networks:
  bridge:

services:
  sdn-rm:
    container_name: sdn-rm
    image: tomcat:8-jdk8
    network_mode: bridge
    container_name: sdn-rm
    hostname: sdn-rm
    labels:
      si.nib.description: "SeaDataNet Replication Manager"
    ports:
      - "8888:8080"
    volumes:
      - sdnrm_rm-home:/home/ReplicationManager
      - sdnrm_tomcat-webapps:/usr/local/tomcat/webapps
      - sdnrm_tomcat-logs:/usr/local/tomcat/logs
volumes:
  sdnrm_rm-home:
  sdnrm_tomcat-webapps:
  sdnrm_tomcat-logs:
```

### Starting the `sdn-rm` container

Finnaly it's time to create and start the container. Here are a few commands for reference:

1. Create or overwrite an `sdn-rm` container and starts it in background: <br/>
  `docker-compose up --force-recreate -d`
- List existing containers (both running and stopped ones): <br/>
  `docker ps -a`
- Start a stopped `sdn-rm` container: <br/>
  `docker start sdn-rm`


## Basic Configuration

### Network ACL

As Tomcat now runs in a container and thus it will have to be exposed to the Internet through a *reverse proxy* or by a *load balancer* such as `HAProxy`, `NGINX`, `Apache2`, `F5 Big-IP` or similar.

This guide outlines required configuration to get it working with `Apache 2.4`. This is the case, as *Apache2* is relatively simple to setup and also it supports things like [ModSecurity](https://modsecurity.org/).

As RM will run behind *Apache2* acting as a reverse proxy However, it is **important** to understand it will see all network requests as coming from the mentioned reverse proxy. Although such deployment is very common, the *RM* package `1.0.46` isn't configured to coupe with it. Therefore the *RM* security configuration should be tweaked a bit. This has to be done:

- Set up the Tomcat `RemoteIpFilter`,  and
- Attach the mentioned filter to all incoming requests.

In order to achieve this, the *RM* `web.xml` residing on the *tomcat-webapps* Docker volume has to be edited - the full path to the file is `<tomcat-webapps>:/ReplicationManager/WEB-INF/web.xml`.

Add following two blocks to the config and restart or just rebuild the *tomcat8* container afterwards:

```xml
...
<!-- X-Forwarded-For filter definition for deployments behind a reverse proxy -->
<filter>
  <filter-name>ReverseProxy_X-Forwarded-For_Filter</filter-name>
  <filter-class>org.apache.catalina.filters.RemoteIpFilter</filter-class>
  <init-param>
    <param-name>remoteIpHeader</param-name>
    <param-value>X-Forwarded-For</param-value>
  </init-param>
  <init-param>
    <param-name>protocolHeader</param-name>
    <param-value>X-Forwarded-Proto</param-value>
  </init-param>
</filter>
...
<!-- X-Forwarded-For filter application for deployments behind a reverse-proxy -->
<filter-mapping>
  <filter-name>ReverseProxy_X-Forwarded-For_Filter</filter-name>
  <url-pattern>/*</url-pattern>
  <dispatcher>REQUEST</dispatcher>
</filter-mapping>
...
```

Of course, the load balancer / reverse proxy should be correctly configurred as well to proper set and handle the `X-Forwarded-For` and `X-Forwarded-Proto` HTTP headers. This will be done later.

For now let's just say the filters mentioned in *RM 1.0.36* installation manual can be used. Another thing to consider and keep in mind is, although *RM* being accessed over `localhost`, Tomcat will see connections coming in from Docker IP, which by default is `172.17.0.1`. Therefore provided access rules should be updated slightly.


### Apache Reverse-Proxy

Now it's time to expose *RM* to Internet **TODO** ...


## References

- Replication Manager web page &ndash; [link](https://www.seadatanet.org/Software/Replication-Manager)
- Direct download link: [Replication Manager 1.0.36](ftp://ftp.ifremer.fr/ifremer/sismer/donnees/SeaDataNet_Software/ReplicationManager_1.0.36.zip)

```bash
docker network inspect bridge | grep Subnet | awk '{ print $2 }' | tr -d '"' | awk -F '.' '{ print $1"\\."$2"\\.\\d+\\.\\d+" }'
```

-> dodaj na MARIS listo (`172\.17\.\d+\.\d+|`)


## Reverse-Proxy Support

In order to deploy the *SDN Replication Manager* behind a reverse proxy, t

```bash
$ rm_home_dir=/srv/SDN_RM/rm-home
$ cat <<__EOF__ |
workspace/readyToSendCDIs/
workspace/tmp/
workspace/queue/
ARCHIVES/
PRODUCTION/
RMDatabase/
RMDatabase/backups/
RMData/data/
RMData/mapping/
conf/externalResources/
__EOF__
while read dir; do
  echo "Creating '${dir}' ..."
  mkdir -p ${rm_home_dir}/${dir}
done
```

In order for Tomcat to log remote client IP addresses, which is passed by the reverse proxy as `X-Forwarded-For` HTTP request header, instead of the local reverse proxy IP address, the Tomcat logging configuration has to be tweaked a bit. For this, update the `server.xml` configuration located within the used Docker container at `/usr/local/tomcat/conf/server.xml`. Replace the `%h` *AccessLogValue* pattern with `%{X-Forwarded-For}i`, so you'll get:

```xml
<Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
      prefix="localhost_access_log" suffix=".txt"
      pattern="%{X-Forwarded-For}i %l %u %t &quot;%r&quot; %s %b" />
```


### Apache config

```apache
 ProxyPreserveHost On
 ProxyAddHeaders   On
 RequestHeader     set   X-Forwarded-Proto   "http"
 ProxyPass         /ReplicationManager     http://127.0.0.1:8888/ReplicationManager
 ProxyPassReverse  /ReplicationManager     http://127.0.0.1:8888/ReplicationManager
```

taddresses instead of the local reverse proxy IP address, the below snipped should be added to Tomcat `server.xml` configuration file to the `<Host>...</Host>` section. The mentioned `server.xml` is located at `/usr/local/tomcat/conf/server.xml` within the Docker container we're using. Here is the configuration snipped to be added:

```xml
<Valve className="org.apache.catalina.valves.RemoteIpValve" />
```
